package main

import (
	"fmt"
	consulapi "github.com/hashicorp/consul/api"
	"log"
	"net/http"
	"os"
	"strconv"
)

func hello(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintf(w, "Hello, %s! from service % port %s", req.URL.Path[1:], envService, envPort)
	//fmt.Println("Hello,", req.URL.Path[1:])
}

func headers(w http.ResponseWriter, req *http.Request) {

	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
			//fmt.Println("Header,", name, h)
		}
	}

}

func health(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintf(w, "ok")
	//fmt.Println("Health,OK")
}
func readiness(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintf(w, "ok")
	//fmt.Println("Readiness,OK")
}

var envPort = ""
var envService=""
func main() {

	//environment
	envPort = os.Getenv("PORT")
	if envPort == "" {
		fmt.Println("please add parameter env PORT")
		os.Exit(1)
	}
	
	envService = os.Getenv("SERVICE")
	if envService == "" {
		fmt.Println("please parameter env SERVICE")
		os.Exit(1)
	}

	registerServiceWithConsul(envPort, envService)
	/*
		//register consul
		client, _ := api.NewClient(api.DefaultConfig())
		// Create an instance representing this service. "my-service" is the
		svc, _ := connect.NewService(envServiceName, client)
		defer svc.Close()
	*/

	fmt.Printf("Service %s running port %s...",  envService, envPort)
	http.HandleFunc("/", hello)
	http.HandleFunc("/health", health)
	http.HandleFunc("/readiness", readiness)
	http.HandleFunc("/headers", headers)
	http.ListenAndServe(":"+envPort,  nil)
}

func registerServiceWithConsul(port, serviceName string) {
	config := consulapi.DefaultConfig()
	consul, err := consulapi.NewClient(config)
	if err != nil {
		log.Fatalln(err)
	}
	registration := new(consulapi.AgentServiceRegistration)
	registration.ID = serviceName+"_"+port   //replace with service id
	registration.Name = serviceName //replace with service name
	address := hostname()
	registration.Address = address
	portInt, _ := strconv.Atoi(port)
	registration.Port = portInt
	registration.Check = new(consulapi.AgentServiceCheck)
	registration.Check.HTTP = fmt.Sprintf("http://%s:%v/health", address, port)
	registration.Check.Interval = "5s"
	registration.Check.Timeout = "3s"
	consul.Agent().ServiceRegister(registration)
}

func hostname() string {
	hn, err := os.Hostname()
	if err != nil {
		log.Fatalln(err)
	}
	return hn
}
